export const CarsComponent = {
    data() {
        return {
            mobil: ['Ferari', 'Porsche', 'Renault','Toyota', 'Suzuki', 'Daihatsu', 'Hyundai', 'Wuling']
        }
    },
    template: `
        <div class='jumbotron'>
            <h1>Daftar Merk Mobil</h1>
            <ul>
			    <li v-for="mr in mobil">
				    {{ mr }}
			    </li>
		    </ul>      
        </div>
    `
}