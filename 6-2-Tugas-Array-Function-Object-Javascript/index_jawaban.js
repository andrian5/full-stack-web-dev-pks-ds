// soal 1
// buatlah variabel seperti di bawah ini

// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

//jawaban soal 1 
console.log('\n')
console.log('Soal 1')
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var hewan = daftarHewan.sort();
    for (var i = 0; i < hewan.length; i++) {
        console.log(hewan[i]);
    }  
console.log('\n')


// soal 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
//jawaban soal 2
function introduce(param) {
    return ("Nama saya " + param.name + ", umur saya " + param.age + " tahun, " + "alamat saya di " + param.address + ", dan saya punya hobby yaitu " + param.hobby)
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)

console.log('Soal 2')
console.log(perkenalan) 
console.log('\n')
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
//jawaban soal 3
function hitung_huruf_vokal(param) { 
    var text = param.toLowerCase();
    var jumlah = 0;
    for(var i=0; i<text.length; i++){
        switch (text[i]){
            case 'a':
                jumlah++
                break;
            case 'i':
                jumlah++
                break;
            case 'u':
                jumlah++
                break;
            case 'e':
                jumlah++
                break;
            case 'o':
                jumlah++
                break;    

            default:
                break;
        }
    }
    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")


console.log('Soal 3')
console.log(hitung_1 , hitung_2) // 3 2
console.log('\n')

// soal 4

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

//jawaban soal 4
function hitung(param) {
    param = -2 + (param * 2)
    return param;
}

console.log('Soal 4')
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8