<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();        
        
        return response()->json([
            'success' => true,
            'message' => 'Data daftar role berhasil ditampilkan',
            'data'    => $roles  
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name'   => 'required',
        ]);
        
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        
        $role = Roles::create([
            'name' => $request->name,
        ]);
        
        if($role)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Role berhasil dibuat',
                'data'    => $role  
            ], 200);    

        }
        
        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::find($id);

        if($role)
        {            
            return response()->json([
                'success' => true,
                'message' => 'Data role berhasil ditampilkan',
                'data'    => $role 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);
        
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Roles::find($id);

        if($role)
        {
            $role->update([
                'name'         => $request->name,
            ]);
        
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diupdate',
                'data'    => $role 
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id :' . $id . 'tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Roles::find($id);

        if($role)
        {
            $role->delete();
            
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
                'data'    => $role 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404); 
    }
}
