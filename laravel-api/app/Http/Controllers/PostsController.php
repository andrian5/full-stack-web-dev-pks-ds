<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{    
    public function __construct()
    {
       return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }

    public function index()
    {
        //get data from table posts
        $posts = Posts::latest()->get();
        
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data daftar Post berhasil ditampilkan',
            'data'    => $posts  
        ]);
    }

    public function store(Request $request)
    {
        //set validation
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //$user = auth()->user;

        //save to database
        $post = Posts::create([
            'title'         => $request->title,
            'description'   => $request->description,
            'user_id'       => $user->id
        ]);

        //success save to database
        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil dibuat',
                'data'    => $post  
            ], 200);    

        }
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        //find post by ID
        $post = Posts::find($id);

        if($post)
        {
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil ditampilkan',
                'data'    => $post 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);       
    }


    public function update(Request $request , $id)
    {
        //set validation
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Posts::find($id);


        if($post)
        {
            $user = auth()->user;

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                    'data'    => $post 
                ], 403); 
            }

            $post->update([
                'title'         => $request->title,
                'description'   => $request->description,
            ]);
        
            return response()->json([
                'success' => true,
                'message' => 'Data dengan judul: ' . $post->title . ' berhasil diupdate',
                'data'    => $post 
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id :' . $id . 'tidak ditemukan',
        ], 404);
    }


    public function destroy($id)
    {
        $post = Posts::find($id);

        if($post)
        {
            $user = auth()->user;

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                    'data'    => $post 
                ], 403); 
            }


            $post->delete();
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
                'data'    => $post 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);  


    }

    
    
    






}