// Soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

//jawaban soal 1
const persegipanjang = (panjang, lebar) => {
    let luas = panjang * lebar;
    let keliling = 2 * (panjang + lebar);
    return {
        luas,
        keliling
    }
}
console.log('\n') 
console.log('Soal 1')
console.log(persegipanjang(5,3)) ;
console.log('\n')


// Soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }

//jawaban soal 2
const newFunction = (firstName, lastName) => {
    firstName,
    lastName,
    fullName(); {
        console.log(fullName)
    }
    
}

// // //Driver Code 
newFunction("William", "Imoh").fullName(); 




// Soal 3
// Diberikan sebuah objek sebagai berikut:
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

//jawaban soal 3
const {firstName, lastName, address, hobby} = newObject
// // Driver code
console.log('Soal 3')
console.log(firstName, lastName, address, hobby)
console.log('\n')


// soal 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code

//jawaban soal 4
const combined = [...west, ...east]
console.log('Soal 4')
console.log(combined)
console.log('\n')



// soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

// const planet = "earth" 
// const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

//jawaban soal 5

const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log('Soal 5')
console.log(before)